#ifndef SERVICE_COM_BACKEND_CSP_REMOTE_H_
#define SERVICE_COM_BACKEND_CSP_REMOTE_H_

#include <stdint.h>

struct csp_remote_backend_handle_t {
  uint8_t node;
  uint8_t ifc_id;
  uint32_t opt_len;
  uint32_t timeout_ms;
};

#endif