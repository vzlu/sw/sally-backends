#include <service/service_com.h>
#include <service/service_err_codes.h>

#include <dev/usart.h>
#include <gpio.h>
#include <sysclk.h>
#include <string.h>

static uint32_t timeout_ms = 0;

int asf_usart_open(service_com_handle_t handle, service_com_device_t dev, service_com_conf_t* conf) {
  if(!handle || !dev || !conf) {
    return(SERVICE_ERR_INVALID_ARG);
  }

  char* dev_str = (char*)dev;
  if((strlen(dev_str) != 6) || (strncmp(dev_str, "usart", 5) != 0)) {
    return(SERVICE_ERR_INVALID_ARG);
  }
  int fd = (int)(dev_str[5] - '0');

  // TODO there has to be a nicer way to do this
  // unfortunately gpio_map_t is typedefed as array ...
  switch(fd) {
    case 1: {
      const gpio_map_t usart_pin_map = {
        {AVR32_USART1_RXD_PIN, AVR32_USART1_RXD_FUNCTION},
        {AVR32_USART1_TXD_PIN, AVR32_USART1_TXD_FUNCTION},
      };
      gpio_enable_module(usart_pin_map, sizeof(usart_pin_map) / sizeof(usart_pin_map[0]));
    } break;

    default:
      return(SERVICE_ERR_INVALID_ARG);
  }

  usart_init(fd, sysclk_get_peripheral_bus_hz(USART), conf->speed);
  *(int*)handle = fd;
  timeout_ms = conf->timeout_ms;
  return(SERVICE_ERR_NONE);
}

int asf_usart_close(service_com_handle_t handle) {
  (void)handle;
  // TODO implement some de-initialization?
  return(SERVICE_ERR_NONE);
}

size_t asf_usart_write(service_com_handle_t handle, const void* buff, size_t count) {
  if(!handle || !buff) {
    return(0);
  }

  int fd = *(int*)handle;

  // the char* makes it look like putstr accepts only C-strings,
  // but the length is not inferred from NULLs, so this is safe
  usart_putstr(fd, (char*)buff, count);
  return(count);
}

size_t asf_usart_read(service_com_handle_t handle, void* buff, size_t count) {
  if(!handle || !buff) {
    return(0);
  }

  int fd = *(int*)handle;
  size_t i = 0;
  char* buffPtr = (char*)buff;
  for(; i < count; i++) {
    int res = usart_getc_timeout(fd, timeout_ms);
    if(res >= 0) {
      buffPtr[i] = res;
    } else {
      // timed out, break
      break;
    }
  }

  return(i);
}

size_t asf_usart_available(service_com_handle_t handle) {
  if(!handle) {
    return(0);
  }

  int fd = *(int*)handle;
  return(usart_messages_waiting(fd));
}

int asf_usart_flush(service_com_handle_t handle) {
  if(!handle) {
    return(SERVICE_ERR_INVALID_ARG);
  }

  int fd = *(int*)handle;
  size_t count = asf_usart_available(handle);
  size_t i = 0;
  for(; i < count; i++) {
    (void)usart_getc_nblock(fd);
  }

  if(count != i) {
    return(SERVICE_ERR_VALUE_OUT_OF_RANGE);
  }

  return(SERVICE_ERR_NONE);
}

service_com_handle_t asf_usart_handle_malloc(void) {
  return((service_com_handle_t*)malloc(sizeof(int)));
}

void asf_usart_handle_free(service_com_handle_t handle) {
  free(handle);
}

service_com_t backend_asf_usart = {
  .open = asf_usart_open,
  .close = asf_usart_close,
  .write = asf_usart_write,
  .read = asf_usart_read,
  .available = asf_usart_available,
  .flush = asf_usart_flush,
  .com_handle_malloc = asf_usart_handle_malloc,
  .com_handle_free = asf_usart_handle_free,
};
