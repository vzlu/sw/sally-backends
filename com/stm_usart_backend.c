#include <service/service_com.h>
#include <service/service_err_codes.h>

#include <uart.h>
#include <stdlib.h>
#include <FreeRTOS.h>
#include <task.h>

int stm_usart_open(service_com_handle_t handle, service_com_device_t dev, service_com_conf_t* conf) {
  (void)dev;
  if(!handle || !conf) {
    return(SERVICE_ERR_INVALID_ARG);
  }

  struct uart_cfg_t* uart = (struct uart_cfg_t*)handle;
  uart->baudRate = conf->speed;
  uart->rxTimeout = conf->timeout_ms;
  if(uart_init(uart)) {
    return(SERVICE_ERR_NONE);
  }
  return(SERVICE_ERR_THREAD_SPAWN_FAILED);
}

int stm_usart_close(service_com_handle_t handle) {
  if(uart_deinit((struct uart_cfg_t*)handle)) {
    return(SERVICE_ERR_NONE);
  }
  return(SERVICE_ERR_THREAD_SPAWN_FAILED);
}

size_t stm_usart_write(service_com_handle_t handle, const void* buff, size_t count) {
  if(!handle || !buff) {
    return(0);
  }

  struct uart_cfg_t* uart = (struct uart_cfg_t*)handle;
  uart_write(uart, (uint8_t*)buff, count);
  return(count);
}

size_t stm_usart_read(service_com_handle_t handle, void* buff, size_t count) {
  if(!handle || !buff) {
    return(0);
  }

  struct uart_cfg_t* uart = (struct uart_cfg_t*)handle;
  size_t i = 0;
  char* buffPtr = (char*)buff;
  for(; i < count; i++) {
    char c = 0;
    char res = uart_getchar(uart, &c);
    if(res > 0) {
      buffPtr[i] = c;
    } else {
      // nothing more to read, break
      break;
    }
  }

  return(i);
}

size_t stm_usart_available(service_com_handle_t handle) {
  if(!handle) {
    return(0);
  }

  struct uart_cfg_t* uart = (struct uart_cfg_t*)handle;
  return(uart_available(uart));
}

int stm_usart_flush(service_com_handle_t handle) {
  if(!handle) {
    return(SERVICE_ERR_INVALID_ARG);
  }

  struct uart_cfg_t* uart = (struct uart_cfg_t*)handle;
  size_t count = stm_usart_available(handle);
  size_t i = 0;
  for(; i < count; i++) {
    char c = 0;
    (void)uart_getchar(uart, &c);
    (void)c;
  }

  if(count != i) {
    return(SERVICE_ERR_VALUE_OUT_OF_RANGE);
  }

  return(SERVICE_ERR_NONE);
}

service_com_handle_t stm_usart_handle_malloc(void) {
  return((service_com_handle_t*)malloc(sizeof(struct uart_cfg_t)));
}

void stm_usart_handle_free(service_com_handle_t handle) {
  free(handle);
}

service_com_t backend_stm_usart = {
  .open = stm_usart_open,
  .close = stm_usart_close,
  .write = stm_usart_write,
  .read = stm_usart_read,
  .available = stm_usart_available,
  .flush = stm_usart_flush,
  .com_handle_malloc = stm_usart_handle_malloc,
  .com_handle_free = stm_usart_handle_free,
};
