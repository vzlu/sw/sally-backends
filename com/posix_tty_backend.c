#include <service/service_com.h>
#include <service/service_err_codes.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/ioctl.h>

int tty_open(service_com_handle_t handle, service_com_device_t dev, service_com_conf_t* conf) {
  int fd = open((char*)dev, O_RDWR | O_NOCTTY | O_SYNC);
  if(fd < 0) {
    return(SERVICE_ERR_INVALID_ARG);
  }

  // TODO how to make this configurable and which parts?
  struct termios options;

  options.c_cflag |= (CLOCAL | CREAD);
  options.c_cflag &= ~CSIZE;
  options.c_cflag |= CS8;
  options.c_cflag &= ~PARENB;
  options.c_cflag &= ~CSTOPB;
  options.c_cflag &= ~CRTSCTS;
  options.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
  options.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
  options.c_oflag &= ~OPOST;

  int should_block = 0;
  options.c_cc[VMIN] = should_block ? 1 : 0;
  options.c_cc[VTIME] = (int)((float)conf->timeout_ms)/100.0f;  // 0.5 seconds read timeout

  // FIXME this should be smarter ...
  switch(conf->speed) {
    case(460800):
      cfsetispeed(&options, B460800);
      cfsetospeed(&options, B460800);
      break;
    case(230400):
      cfsetispeed(&options, B230400);
      cfsetospeed(&options, B230400);
      break;
    case(115200):
      cfsetispeed(&options, B115200);
      cfsetospeed(&options, B115200);
      break;
    case(57600):
      cfsetispeed(&options, B57600);
      cfsetospeed(&options, B57600);
      break;
    case(38400):
      cfsetispeed(&options, B38400);
      cfsetospeed(&options, B38400);
      break;
    case(19200):
      cfsetispeed(&options, B19200);
      cfsetospeed(&options, B19200);
      break;
    case(9600):
      cfsetispeed(&options, B9600);
      cfsetospeed(&options, B9600);
      break;
    default:
      cfsetispeed(&options, B115200);
      cfsetospeed(&options, B115200);
      break;
  }

  tcflush(fd, TCIFLUSH);
  tcsetattr(fd, TCSANOW, &options);

  *(int*)handle = fd;
  return(SERVICE_ERR_NONE);
}

int tty_close(service_com_handle_t handle) {
  int fd = *(int*)handle;
  return(close(fd));
}

size_t tty_write(service_com_handle_t handle, const void* buff, size_t count) {
  int fd = *(int*)handle;
  ssize_t written = write(fd, buff, count);
  if(written < 0) {
    return(0);
  }
  return((size_t)written);
}

size_t tty_read(service_com_handle_t handle, void* buff, size_t count) {
  int fd = *(int*)handle;
  size_t total = 0;
  size_t rem = count;
  uint8_t* buff_ptr = (uint8_t*)buff;
  while(total < count) {
    ssize_t rd = read(fd, &buff_ptr[total], rem);
    if(rd <= 0) {
      break;
    }
    total += rd;
    rem -= rd;
  }
  return(total);
}

size_t tty_available(service_com_handle_t handle) {
  int fd = *(int*)handle;
  int bytes;
  // Note: in some systems, ioctl could return a non-zero value
  if (ioctl(fd, FIONREAD, &bytes) == 0) {
    return bytes;
  }
  return 0;
}

int tty_flush(service_com_handle_t handle) {
  int fd = *(int*)handle;
  return(tcflush(fd, TCIFLUSH));
}

service_com_handle_t tty_handle_malloc(void) {
  return((service_com_handle_t*)malloc(sizeof(int)));
}

void tty_handle_free(service_com_handle_t handle) {
  free(handle);
}

service_com_t backend_posix_tty = {
  .open = tty_open,
  .close = tty_close,
  .write = tty_write,
  .read = tty_read,
  .available = tty_available,
  .flush = tty_flush,
  .com_handle_malloc = tty_handle_malloc,
  .com_handle_free = tty_handle_free,
};
