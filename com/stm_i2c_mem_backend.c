#include <service/service_com.h>
#include <service/service_err_codes.h>

#include <i2c.h>
#include <stdlib.h>
#include <stdbool.h>

// ah yes, "inheritance" in C ... what could go wrong
extern int stm_i2c_open(service_com_handle_t handle, service_com_device_t dev, service_com_conf_t* conf);
extern int stm_i2c_close(service_com_handle_t handle);
extern size_t stm_i2c_available(service_com_handle_t handle);
extern int stm_i2c_flush(service_com_handle_t handle);
extern service_com_handle_t stm_i2c_handle_malloc(void);
extern void stm_i2c_handle_free(service_com_handle_t handle);

size_t stm_i2c_mem_write(service_com_handle_t handle, const void* buff, size_t count) {
  if(!handle || !buff) {
    return(0);
  }

  struct i2c_cfg_t* i2c = (struct i2c_cfg_t*)handle;
  uint8_t* data = (uint8_t*)buff;

  // assume the first byte is slave I2C address and the second is register address
  if(!i2c_transfer_mem(i2c, data[0], true, data[1], &data[2], count)) {
    return(0);
  }
  return(count);
}

size_t stm_i2c_mem_read(service_com_handle_t handle, void* buff, size_t count) {
  if(!handle || !buff) {
    return(0);
  }

  struct i2c_cfg_t* i2c = (struct i2c_cfg_t*)handle;
  uint8_t* data = (uint8_t*)buff;

  // assume the first byte is slave I2C address and the second is register address
  if(!i2c_transfer_mem(i2c, data[0], false, data[1], &data[2], count)) {
    return(0);
  }
  return(count);
}

service_com_t backend_stm_i2c_mem = {
  .open = stm_i2c_open,
  .close = stm_i2c_close,
  .write = stm_i2c_mem_write,
  .read = stm_i2c_mem_read,
  .available = stm_i2c_available, // for memory access, this will always return 0
  .flush = stm_i2c_flush,
  .com_handle_malloc = stm_i2c_handle_malloc,
  .com_handle_free = stm_i2c_handle_free,
};
