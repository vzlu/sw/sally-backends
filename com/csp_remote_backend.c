#include "csp_remote_backend.h"

#include <service/service_com.h>
#include <service/service_err_codes.h>

#include <ifacer/ifacer_client.h>
#include <console/cmddef.h>

#include <stdlib.h>
#include <string.h>

static void csp_remote_set_last_err(int err);

int csp_remote_open(service_com_handle_t handle, service_com_device_t dev, service_com_conf_t* conf) {
  if(!handle || !dev || !conf) {
    return(SERVICE_ERR_INVALID_ARG);
  }

  // expected format is XX:YY
  char* dev_str = (char*)dev;
  if((strlen(dev_str) != 5) || (dev_str[2] != ':')) {
    return(SERVICE_ERR_INVALID_ARG);
  }

  struct csp_remote_backend_handle_t* hcom = (struct csp_remote_backend_handle_t*)handle;
  hcom->node = strtol(&dev_str[0], NULL, 10);
  hcom->ifc_id = strtol(&dev_str[3], NULL, 10);
  hcom->timeout_ms = conf->timeout_ms;
  return(ifacer_ifc_config(hcom->node, hcom->ifc_id, true, conf->speed, hcom->timeout_ms, NULL));
}

int csp_remote_close(service_com_handle_t handle) {
  struct csp_remote_backend_handle_t* hcom = (struct csp_remote_backend_handle_t*)handle;
  return(ifacer_ifc_config(hcom->node, hcom->ifc_id, false, 0, 0, NULL));
}

size_t csp_remote_write(service_com_handle_t handle, const void* buff, size_t count) {
  if(!handle || !buff) {
    return(0);
  }

  struct csp_remote_backend_handle_t* hcom = (struct csp_remote_backend_handle_t*)handle;
  int16_t ret = ifacer_ifc_write(hcom->node, hcom->ifc_id, (void*)buff, count, NULL);
  if(ret != SERVICE_ERR_NONE) {
    csp_remote_set_last_err(ret);
    return(0);
  }
  return(count);
}

size_t csp_remote_read(service_com_handle_t handle, void* buff, size_t count) {
  if(!handle || !buff) {
    return(0);
  }

  struct csp_remote_backend_handle_t* hcom = (struct csp_remote_backend_handle_t*)handle;
  struct Interfacer_Read_Reply* rpl = NULL;
  int16_t ret = ifacer_ifc_read(hcom->node, hcom->ifc_id, count, hcom->timeout_ms, buff, hcom->opt_len, &rpl);
  if(ret != SERVICE_ERR_NONE) {
    if(rpl) { console_free(rpl); }
    csp_remote_set_last_err(ret);
    return(0);
  }

  if(!rpl) {
    return(0);
  }

  size_t rpl_len = rpl->bytes_len;
  memcpy(buff, rpl->bytes, rpl_len);
  if(rpl) { console_free(rpl); }
  return(rpl_len);
}

size_t csp_remote_available(service_com_handle_t handle) {
  if(!handle) {
    return(0);
  }

  struct csp_remote_backend_handle_t* hcom = (struct csp_remote_backend_handle_t*)handle;
  struct Interfacer_Available_Reply* rpl = NULL;
  int16_t ret = ifacer_ifc_available(hcom->node, hcom->ifc_id, &rpl);
  if(ret != SERVICE_ERR_NONE) {
    if(rpl) { console_free(rpl); }
    csp_remote_set_last_err(ret);
    return(0);
  }

  if(!rpl) {
    return(0);
  }

  size_t av = rpl->available;
  if(rpl) { console_free(rpl); }
  return(av);
}

int csp_remote_flush(service_com_handle_t handle) {
  if(!handle) {
    return(SERVICE_ERR_INVALID_ARG);
  }

  struct csp_remote_backend_handle_t* hcom = (struct csp_remote_backend_handle_t*)handle;
  return(ifacer_ifc_flush(hcom->node, hcom->ifc_id, NULL));
}

service_com_handle_t csp_remote_handle_malloc(void) {
  return((service_com_handle_t*)malloc(sizeof(struct csp_remote_backend_handle_t)));
}

void csp_remote_handle_free(service_com_handle_t handle) {
  free(handle);
}

service_com_t backend_csp_remote = {
  .last_err = 0,
  .open = csp_remote_open,
  .close = csp_remote_close,
  .write = csp_remote_write,
  .read = csp_remote_read,
  .available = csp_remote_available,
  .flush = csp_remote_flush,
  .com_handle_malloc = csp_remote_handle_malloc,
  .com_handle_free = csp_remote_handle_free,
};

static void csp_remote_set_last_err(int err) {
  backend_csp_remote.last_err = err;
}
