#include <service/service_com.h>
#include <service/service_err_codes.h>

#include <i2c.h>
#include <task.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

int stm_i2c_open(service_com_handle_t handle, service_com_device_t dev, service_com_conf_t* conf) {
  (void)dev;
  if(!handle || !conf) {
    return(SERVICE_ERR_INVALID_ARG);
  }

  struct i2c_cfg_t* i2c = (struct i2c_cfg_t*)handle;
  i2c->freq = conf->speed;
  i2c->txTimeout = conf->timeout_ms;
  i2c_init(i2c);
  return(SERVICE_ERR_NONE);
}

int stm_i2c_close(service_com_handle_t handle) {
  i2c_deinit((struct i2c_cfg_t*)handle);
  return(SERVICE_ERR_NONE);
}

size_t stm_i2c_write(service_com_handle_t handle, const void* buff, size_t count) {
  if(!handle || !buff) {
    return(0);
  }

  struct i2c_cfg_t* i2c = (struct i2c_cfg_t*)handle;
  uint8_t* data = (uint8_t*)buff;

  // assume the first byte is slave I2C address
  if(!i2c_transfer(i2c, data[0], true, &data[1], count)) {
    return(0);
  }
  return(count);
}

size_t stm_i2c_read(service_com_handle_t handle, void* buff, size_t count) {
  if(!handle || !buff) {
    return(0);
  }

  struct i2c_cfg_t* i2c = (struct i2c_cfg_t*)handle;
  uint8_t* data = (uint8_t*)buff;

  // assume the first byte is slave I2C address
  if(!i2c_transfer(i2c, data[0], false, &data[1], count)) {
    return(0);
  }

  // check if timeout is set - if so then block
  if(i2c->txTimeout) {
    uint32_t start = xTaskGetTickCount();
    while(i2c->rxLen < count) {
      vTaskDelay(1);
      if(xTaskGetTickCount() - start >= i2c->txTimeout) {
        return(0);
      }
    }
  }

  memcpy(&data[1], i2c->rxBuffPtr, i2c->rxLen);
  return(i2c->rxLen);
}

size_t stm_i2c_available(service_com_handle_t handle) {
  if(!handle) {
    return(0);
  }

  struct i2c_cfg_t* i2c = (struct i2c_cfg_t*)handle;
  return(i2c->rxLen);
}

int stm_i2c_flush(service_com_handle_t handle) {
  if(!handle) {
    return(SERVICE_ERR_INVALID_ARG);
  }

  struct i2c_cfg_t* i2c = (struct i2c_cfg_t*)handle;
  i2c->rxLen = 0;

  return(SERVICE_ERR_NONE);
}

service_com_handle_t stm_i2c_handle_malloc(void) {
  return((service_com_handle_t*)malloc(sizeof(struct i2c_cfg_t)));
}

void stm_i2c_handle_free(service_com_handle_t handle) {
  free(handle);
}

service_com_t backend_stm_i2c = {
  .open = stm_i2c_open,
  .close = stm_i2c_close,
  .write = stm_i2c_write,
  .read = stm_i2c_read,
  .available = stm_i2c_available,
  .flush = stm_i2c_flush,
  .com_handle_malloc = stm_i2c_handle_malloc,
  .com_handle_free = stm_i2c_handle_free,
};
