#ifndef SERVICE_OS_BACKEND_FREERTOS_H_
#define SERVICE_OS_BACKEND_FREERTOS_H_

#include "FreeRTOS.h"
#define pdMS_TO_TICKS( xTimeInMs ) ( ( TickType_t ) ( ( ( TickType_t ) ( xTimeInMs ) * ( TickType_t ) configTICK_RATE_HZ ) / ( TickType_t ) 1000 ) )

struct freertos_thread_attr_t {
  const char* pcName;
  uint32_t usStackDepth;
  UBaseType_t uxPriority;
};

#endif